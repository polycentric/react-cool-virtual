import { useRef, useCallback, useLayoutEffect, useEffect, useState } from 'react';

// Internal
// External
var Align;

(function (Align) {
  Align["auto"] = "auto";
  Align["start"] = "start";
  Align["center"] = "center";
  Align["end"] = "end";
})(Align || (Align = {}));

var findNearestBinarySearch = (function (low, high, input, getVal) {
  while (low <= high) {
    var mid = (low + high) / 2 | 0;
    var val = getVal(mid);

    if (input < val) {
      high = mid - 1;
    } else if (input > val) {
      low = mid + 1;
    } else {
      return mid;
    }
  }

  return low > 0 ? low - 1 : 0;
});

var isNumber = (function (val) {
  return typeof val === "number" && !Number.isNaN(val);
});

var now = (function () {
  return (// eslint-disable-next-line compat/compat
    "performance" in window ? performance.now() : Date.now()
  );
});

var shouldUpdate = (function (prev, next, skip) {
  if (prev.length !== next.length) return true;

  var _loop = function _loop(i) {
    if (Object.keys(prev[i]).some(function (key) {
      var k = key;
      return !skip[k] && prev[i][k] !== next[i][k];
    })) return {
      v: true
    };
  };

  for (var i = 0; i < prev.length; i += 1) {
    var _ret = _loop(i);

    if (typeof _ret === "object") return _ret.v;
  }

  return false;
});

var useLatest = (function (val) {
  var ref = useRef(val);
  ref.current = val;
  return ref;
});

var useDebounce = (function (cb, delay) {
  var rafRef = useRef();
  var cbRef = useLatest(cb);
  var cancel = useCallback(function () {
    if (rafRef.current) {
      cancelAnimationFrame(rafRef.current);
      rafRef.current = undefined;
    }
  }, []);
  var tick = useCallback(function (start) {
    if (now() - start >= delay) {
      cbRef.current();
    } else {
      rafRef.current = requestAnimationFrame(function () {
        return tick(start);
      });
    }
  }, [cbRef, delay]);
  var fn = useCallback(function () {
    cancel();
    tick(now());
  }, [cancel, tick]);
  return [fn, cancel];
});

var useIsoLayoutEffect = typeof window !== "undefined" ? useLayoutEffect : useEffect;

var useResizeEffect = (function (ref, cb, deps) {
  var cbRef = useLatest(cb);
  useIsoLayoutEffect(function () {
    if (!(ref != null && ref.current)) return function () {
      return null;
    }; // eslint-disable-next-line compat/compat

    var observer = new ResizeObserver(function (_ref) {
      var contentRect = _ref[0].contentRect;
      var width = contentRect.width,
          height = contentRect.height;
      cbRef.current({
        width: width,
        height: height
      });
    });
    observer.observe(ref.current);
    return function () {
      return observer.disconnect();
    };
  }, [cbRef, ref].concat(deps));
});

var getInitState = function getInitState(itemSize, ssrItemCount) {
  if (ssrItemCount === void 0) {
    ssrItemCount = 0;
  }

  var _ref = isNumber(ssrItemCount) ? [0, ssrItemCount - 1] : ssrItemCount,
      idx = _ref[0],
      len = _ref[1];

  var items = [];

  for (var i = idx; i <= len; i += 1) {
    items.push({
      index: i,
      start: 0,
      width: 0,
      size: isNumber(itemSize) ? itemSize : itemSize(i, 0),
      measureRef:
      /* istanbul ignore next */
      function measureRef() {
        return null;
      }
    });
  }

  return {
    items: items
  };
};

var useVirtual = (function (_ref2) {
  var itemCount = _ref2.itemCount,
      ssrItemCount = _ref2.ssrItemCount,
      _ref2$itemSize = _ref2.itemSize,
      itemSize = _ref2$itemSize === void 0 ? 50 : _ref2$itemSize,
      horizontal = _ref2.horizontal,
      resetScroll = _ref2.resetScroll,
      _ref2$overscanCount = _ref2.overscanCount,
      overscanCount = _ref2$overscanCount === void 0 ? 1 : _ref2$overscanCount,
      useIsScrolling = _ref2.useIsScrolling,
      stickyIndices = _ref2.stickyIndices,
      _ref2$scrollDuration = _ref2.scrollDuration,
      scrollDuration = _ref2$scrollDuration === void 0 ? function (d) {
    return Math.min(Math.max(d * 0.075, 100), 500);
  } : _ref2$scrollDuration,
      _ref2$scrollEasingFun = _ref2.scrollEasingFunction,
      scrollEasingFunction = _ref2$scrollEasingFun === void 0 ? function (t) {
    return -(Math.cos(Math.PI * t) - 1) / 2;
  } : _ref2$scrollEasingFun,
      _ref2$loadMoreCount = _ref2.loadMoreCount,
      loadMoreCount = _ref2$loadMoreCount === void 0 ? 15 : _ref2$loadMoreCount,
      isItemLoaded = _ref2.isItemLoaded,
      loadMore = _ref2.loadMore,
      onScroll = _ref2.onScroll,
      onResize = _ref2.onResize;

  var _useState = useState(function () {
    return getInitState(itemSize, ssrItemCount);
  }),
      state = _useState[0],
      setState = _useState[1];

  var isMountedRef = useRef(false);
  var isScrollingRef = useRef(true);
  var isScrollToItemRef = useRef(false);
  var hasDynamicSizeRef = useRef(false);
  var rosRef = useRef(new Map());
  var scrollOffsetRef = useRef(0);
  var prevItemIdxRef = useRef(-1);
  var prevVStopRef = useRef(-1);
  var outerRef = useRef(null);
  var innerRef = useRef(null);
  var outerRectRef = useRef({
    width: 0,
    height: 0
  });
  var msDataRef = useRef([]);
  var userScrollRef = useRef(true);
  var scrollToRafRef = useRef();
  var stickyIndicesRef = useLatest(stickyIndices);
  var durationRef = useLatest(scrollDuration);
  var easingFnRef = useLatest(scrollEasingFunction);
  var isItemLoadedRef = useLatest(isItemLoaded);
  var loadMoreRef = useLatest(loadMore);
  var itemSizeRef = useLatest(itemSize);
  var useIsScrollingRef = useLatest(useIsScrolling);
  var onScrollRef = useLatest(onScroll);
  var onResizeRef = useLatest(onResize);
  var sizeKey = !horizontal ? "height" : "width";
  var marginKey = !horizontal ? "marginTop" : "marginLeft";
  var scrollKey = !horizontal ? "scrollTop" : "scrollLeft";
  var getItemSize = useCallback(function (idx) {
    var size = itemSizeRef.current;
    return isNumber(size) ? size : size(idx, outerRectRef.current.width);
  }, [itemSizeRef]);
  var getMeasure = useCallback(function (idx, size) {
    var _msDataRef$current$en, _msDataRef$current;

    var start = (_msDataRef$current$en = (_msDataRef$current = msDataRef.current[idx - 1]) == null ? void 0 : _msDataRef$current.end) != null ? _msDataRef$current$en : 0;
    return {
      idx: idx,
      start: start,
      end: start + size,
      size: size
    };
  }, []);
  var measureItems = useCallback(function (useCache) {
    if (useCache === void 0) {
      useCache = true;
    }

    msDataRef.current.length = itemCount;

    for (var i = 0; i < itemCount; i += 1) {
      msDataRef.current[i] = getMeasure(i, useCache && msDataRef.current[i] ? msDataRef.current[i].size : getItemSize(i));
    }
  }, [getItemSize, getMeasure, itemCount]);
  var getCalcData = useCallback(function (scrollOffset) {
    var msData = msDataRef.current;
    var lastIdx = msData.length - 1;
    var vStart = 0;

    if (hasDynamicSizeRef.current) {
      while (vStart < lastIdx && msData[vStart].start + msData[vStart].size < scrollOffset) {
        vStart += 1;
      }
    } else {
      vStart = findNearestBinarySearch(0, lastIdx, scrollOffset, function (idx) {
        return msData[idx].start;
      });
    }

    var vStop = vStart;
    var currStart = msData[vStop].start;

    while (vStop < lastIdx && currStart < scrollOffset + outerRectRef.current[sizeKey]) {
      currStart += msData[vStop].size;
      vStop += 1;
    }

    vStop = vStop === lastIdx ? vStop : vStop - 1;
    var oStart = Math.max(vStart - overscanCount, 0);
    var oStop = Math.min(vStop + overscanCount, lastIdx);
    var innerMargin = msData[oStart].start;
    var totalSize = Math[oStop < lastIdx ? "max" : "min"](msData[oStop].end + msData[oStop].size, msData[lastIdx].end);
    return {
      oStart: oStart,
      oStop: oStop,
      vStart: vStart,
      vStop: vStop,
      innerMargin: innerMargin,
      innerSize: totalSize - innerMargin
    };
  }, [overscanCount, sizeKey]);
  var scrollTo = useCallback(function (offset, isScrolling) {
    if (isScrolling === void 0) {
      isScrolling = true;
    }

    if (outerRef.current) {
      userScrollRef.current = false;
      isScrollingRef.current = isScrolling;
      outerRef.current[scrollKey] = offset;
    }
  }, [scrollKey]);
  var scrollToOffset = useCallback(function (val, cb) {
    var _ref3 = isNumber(val) ? {
      offset: val
    } : val,
        offset = _ref3.offset,
        smooth = _ref3.smooth;

    if (!isNumber(offset)) return;

    if (!smooth) {
      scrollTo(offset);
      if (cb) cb();
      return;
    }

    var prevOffset = scrollOffsetRef.current;
    var start = now();

    var scroll = function scroll() {
      var duration = durationRef.current;
      duration = isNumber(duration) ? duration : duration(Math.abs(offset - prevOffset));
      var time = Math.min((now() - start) / duration, 1);
      var easing = easingFnRef.current(time);
      scrollTo(easing * (offset - prevOffset) + prevOffset);

      if (time < 1) {
        scrollToRafRef.current = requestAnimationFrame(scroll);
      } else if (cb) {
        cb();
      }
    };

    if (window.requestAnimationFrame) {
      scrollToRafRef.current = window.requestAnimationFrame(scroll);
    } else {
      // Fallback for browsers that do not support requestAnimationFrame
      scrollToRafRef.current = window.setTimeout(scroll, 16);
    }
  }, [durationRef, easingFnRef, scrollTo]);
  var scrollToIndex = useCallback(function (val, cb, isSync) {
    var _ref4 = isNumber(val) ? {
      index: val
    } : val,
        index = _ref4.index,
        _ref4$align = _ref4.align,
        align = _ref4$align === void 0 ? Align.auto : _ref4$align,
        smooth = _ref4.smooth;

    if (!isNumber(index)) return;
    isScrollToItemRef.current = true; // For dynamic size, we must measure it for getting the correct scroll position

    if (hasDynamicSizeRef.current) measureItems();
    var msData = msDataRef.current;
    var ms = msData[Math.max(0, Math.min(index, msData.length - 1))];
    if (!ms) return;
    var start = ms.start,
        end = ms.end,
        size = ms.size;
    var totalSize = msData[msData.length - 1].end;
    var outerSize = outerRectRef.current[sizeKey];
    var scrollOffset = scrollOffsetRef.current;

    if (totalSize <= outerSize) {
      if (cb) cb();
      return;
    }

    if (isSync || align === Align.start || align === Align.auto && scrollOffset + outerSize > end && scrollOffset > start) {
      scrollOffset = totalSize - start <= outerSize ? totalSize - outerSize : start;
    } else if (align === Align.end || align === Align.auto && scrollOffset + outerSize < end && scrollOffset < start) {
      scrollOffset = start + size <= outerSize ? 0 : start - outerSize + size;
    } else if (align === Align.center && start + size / 2 > outerSize / 2) {
      var to = start - outerSize / 2 + size / 2;
      scrollOffset = totalSize - to <= outerSize ? totalSize - outerSize : to;
    }

    if (hasDynamicSizeRef.current && Math.abs(scrollOffset - scrollOffsetRef.current) <= 1) {
      if (cb) cb();
      return;
    }

    scrollToOffset({
      offset: scrollOffset,
      smooth: smooth
    }, function () {
      if (!hasDynamicSizeRef.current) {
        if (cb) cb();
      } else if (isSync) {
        requestAnimationFrame(function () {
          return scrollToIndex(val, cb, isSync);
        });
      } else {
        setTimeout(function () {
          return scrollToIndex(val, cb);
        });
      }
    });
  }, [measureItems, scrollToOffset, sizeKey]);
  var scrollToItem = useCallback(function (val, cb) {
    return scrollToIndex(val, cb);
  }, [scrollToIndex]);
  var startItem = useCallback(function (idx, cb) {
    return scrollToIndex(idx, cb, true);
  }, [scrollToIndex]);

  var _useDebounce = useDebounce( // eslint-disable-next-line @typescript-eslint/no-use-before-define
  function () {
    return handleScroll(scrollOffsetRef.current);
  }, 150),
      resetIsScrolling = _useDebounce[0],
      cancelResetIsScrolling = _useDebounce[1];

  var handleScroll = useCallback(function (scrollOffset, isScrolling, uxScrolling) {
    if (loadMoreRef.current && !isMountedRef.current && !(isItemLoadedRef.current && isItemLoadedRef.current(0))) loadMoreRef.current({
      startIndex: 0,
      stopIndex: loadMoreCount - 1,
      loadIndex: 0,
      scrollOffset: scrollOffset,
      userScroll: false
    });

    if (!itemCount) {
      setState({
        items: []
      });
      return;
    }

    var calcData = getCalcData(scrollOffset);
    var oStart = calcData.oStart,
        oStop = calcData.oStop,
        vStart = calcData.vStart,
        vStop = calcData.vStop;
    var innerMargin = calcData.innerMargin,
        innerSize = calcData.innerSize;
    var items = [];
    var stickies = Array.isArray(stickyIndicesRef.current) ? stickyIndicesRef.current : [];

    var _loop = function _loop(i) {
      var msData = msDataRef.current;
      var _msData$i = msData[i],
          start = _msData$i.start,
          size = _msData$i.size;
      items.push({
        index: i,
        start: start - innerMargin,
        size: size,
        width: outerRectRef.current.width,
        isScrolling: uxScrolling || undefined,
        isSticky: stickies.includes(i) || undefined,
        measureRef: function measureRef(el) {
          if (!el) return; // eslint-disable-next-line compat/compat

          new ResizeObserver(function (_ref5, ro) {
            var _msData$end, _msData, _outerRectRef$current, _rosRef$current$get;

            var target = _ref5[0].target;
            // NOTE: Use `borderBoxSize` when it's supported by Safari
            // see: https://caniuse.com/mdn-api_resizeobserverentry_borderboxsize
            var measuredSize = target.getBoundingClientRect()[sizeKey];

            if (!measuredSize) {
              ro.disconnect();
              rosRef.current["delete"](target);
              return;
            }

            var prevEnd = (_msData$end = (_msData = msData[i - 1]) == null ? void 0 : _msData.end) != null ? _msData$end : 0;
            var viewportHeight = ((_outerRectRef$current = outerRectRef.current) == null ? void 0 : _outerRectRef$current.height) || window.innerHeight;

            if (measuredSize !== size || start !== prevEnd) {
              // To prevent dynamic size from jumping during backward scrolling
              if (i < prevItemIdxRef.current && start < scrollOffset && start + measuredSize < scrollOffset - viewportHeight) {
                scrollTo(scrollOffset + measuredSize - size, false);
              }

              msDataRef.current[i] = getMeasure(i, measuredSize);
              if (!isScrollToItemRef.current) handleScroll(scrollOffsetRef.current, isScrolling, uxScrolling);
              hasDynamicSizeRef.current = true;
            }

            prevItemIdxRef.current = i;
            (_rosRef$current$get = rosRef.current.get(target)) == null ? void 0 : _rosRef$current$get.disconnect();
            rosRef.current.set(target, ro);
          }).observe(el);
        }
      });
    };

    for (var i = oStart; i <= oStop; i += 1) {
      _loop(i);
    }

    if (stickies.length) {
      var stickyIdx = stickies[findNearestBinarySearch(0, stickies.length - 1, vStart, function (idx) {
        return stickies[idx];
      })];

      if (oStart > stickyIdx) {
        var size = msDataRef.current[stickyIdx].size;
        items.unshift({
          index: stickyIdx,
          start: 0,
          size: size,
          width: outerRectRef.current.width,
          isScrolling: uxScrolling || undefined,
          isSticky: true,
          measureRef:
          /* istanbul ignore next */
          function measureRef() {
            return null;
          }
        });
        innerMargin -= size;
        innerSize += size;
      }
    }

    setState(function (prevState) {
      return shouldUpdate(prevState.items, items, {
        measureRef: true
      }) ? {
        items: items,
        innerMargin: innerMargin,
        innerSize: innerSize
      } : prevState;
    });
    if (!isScrolling) return;
    var scrollForward = scrollOffset > scrollOffsetRef.current;
    if (onScrollRef.current) onScrollRef.current({
      overscanStartIndex: oStart,
      overscanStopIndex: oStop,
      visibleStartIndex: vStart,
      visibleStopIndex: vStop,
      scrollOffset: scrollOffset,
      scrollForward: scrollForward,
      userScroll: userScrollRef.current
    });
    var loadIndex = Math.max(Math.floor((vStop + 1) / loadMoreCount) - (scrollForward ? 0 : 1), 0);
    var startIndex = loadIndex * loadMoreCount;
    if (loadMoreRef.current && vStop !== prevVStopRef.current && !(isItemLoadedRef.current && isItemLoadedRef.current(loadIndex))) loadMoreRef.current({
      startIndex: startIndex,
      stopIndex: startIndex + loadMoreCount - 1,
      loadIndex: loadIndex,
      scrollOffset: scrollOffset,
      userScroll: userScrollRef.current
    });
    if (uxScrolling) resetIsScrolling();
    prevVStopRef.current = vStop;
  }, [stickyIndicesRef, getCalcData, getMeasure, itemCount, loadMoreCount, loadMoreRef, onScrollRef, resetIsScrolling, scrollTo, sizeKey, isItemLoadedRef]);
  useResizeEffect(outerRef, function (rect) {
    var _msDataRef$current2;

    var _outerRectRef$current2 = outerRectRef.current,
        width = _outerRectRef$current2.width,
        height = _outerRectRef$current2.height;
    var isSameWidth = width === rect.width;
    var isSameSize = isSameWidth && height === rect.height;
    var msDataLen = msDataRef.current.length;
    var prevTotalSize = (_msDataRef$current2 = msDataRef.current[msDataLen - 1]) == null ? void 0 : _msDataRef$current2.end;
    outerRectRef.current = rect;
    measureItems(hasDynamicSizeRef.current);
    handleScroll(scrollOffsetRef.current);
    if (resetScroll && itemCount !== msDataLen) setTimeout(function () {
      return scrollTo(0, false);
    });

    if (!isMountedRef.current) {
      isMountedRef.current = true;
      return;
    }

    if (!hasDynamicSizeRef.current && !isSameWidth) {
      var _msDataRef$current3;

      var totalSize = (_msDataRef$current3 = msDataRef.current[msDataRef.current.length - 1]) == null ? void 0 : _msDataRef$current3.end;
      var ratio = totalSize / prevTotalSize || 1;
      scrollTo(scrollOffsetRef.current * ratio, false);
    }

    if (!isSameSize && onResizeRef.current) onResizeRef.current(rect);
  }, [itemCount, resetScroll, handleScroll, measureItems, onResizeRef, scrollTo]);
  useIsoLayoutEffect(function () {
    if (!innerRef.current) return;
    if (isNumber(state.innerMargin)) innerRef.current.style[marginKey] = state.innerMargin + "px";
    if (isNumber(state.innerSize)) innerRef.current.style[sizeKey] = state.innerSize + "px";
  }, [marginKey, sizeKey, state.innerMargin, state.innerSize]);
  useIsoLayoutEffect(function () {
    var outer = outerRef.current;
    if (!outer) return function () {
      return null;
    };

    var scrollHandler = function scrollHandler(_ref6) {
      var target = _ref6.target;
      var scrollOffset = target[scrollKey];
      if (scrollOffset === scrollOffsetRef.current) return;
      var uxScrolling = useIsScrollingRef.current;
      uxScrolling = typeof uxScrolling === "function" ? uxScrolling(Math.abs(scrollOffset - scrollOffsetRef.current)) : uxScrolling;
      handleScroll(scrollOffset, isScrollingRef.current, uxScrolling);
      userScrollRef.current = true;
      isScrollingRef.current = true;
      isScrollToItemRef.current = false;
      scrollOffsetRef.current = scrollOffset;
    };

    outer.addEventListener("scroll", scrollHandler, {
      passive: true
    });
    var ros = rosRef.current;
    return function () {
      cancelResetIsScrolling();

      if (scrollToRafRef.current) {
        cancelAnimationFrame(scrollToRafRef.current);
        scrollToRafRef.current = undefined;
      }

      outer.removeEventListener("scroll", scrollHandler);
      ros.forEach(function (ro) {
        return ro.disconnect();
      });
      ros.clear();
    };
  }, [cancelResetIsScrolling, handleScroll, scrollKey, useIsScrollingRef]);
  return {
    outerRef: outerRef,
    innerRef: innerRef,
    items: state.items,
    scrollTo: scrollToOffset,
    scrollToItem: scrollToItem,
    startItem: startItem
  };
});

export { useVirtual as default };
//# sourceMappingURL=index.esm.js.map
